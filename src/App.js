import React, { Component } from 'react';
import './App.css';
import fire from './config/Fire'
import Login from './LoginOtp';
import Home from './Home'


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      user:{}
    }
  }

  componentDidMount(){
    this.authListener();
  }

  authListener(){
      fire.auth().onAuthStateChanged((user)=>{
          if(user){
            this.setState({user});
          }else{
            this.setState({user:null});
          }
      })
  }

  render(){
    console.log("state app", this.state)
    return (
      <div className="App">
        {this.state.user ? (<Home/>) : (<Login/>)}
      </div>
    );
  }
}

export default App;
