import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import fire from './config/Fire';
import * as firebase from 'firebase'

let cek = null
class Login extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.verify = this.verify.bind(this);
    this.renderMessage = this.renderMessage.bind(this);

    this.state = {
      user:null,
      phoneNumber: '',
      recaptcha: false,
      confirmationResult:null,
      code: '',
      visible:false,
      message: ''
    };
  }

  componentDidMount() {

    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(this.recaptcha, {
      'size': 'normal',
      'callback':  (response) => {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        // response.setHeader("Set-Cookie", "HttpOnly;Secure;SameSite=Strict"); 
        // ...
        this.setState({
          recaptcha:true
        })
      },
      'expired-callback': () => {
        // Response expired. Ask user to solve reCAPTCHA again.
        // ...
      }
    });

    window.recaptchaVerifier.render().then( widgetId=> {
      window.recaptchaWidgetId = widgetId;
    });
  }

  handleChange(e) {
    console.log("apa", e)
    this.setState({ [e.target.name]: e.target.value });
  }


  handleVerify(e){
    this.setState({ code: e.target.value })
  }

  login(e) {
    e.preventDefault();

    this.setState({
      message:"send otp"
    })

    
    // // Turn off phone auth app verification.
    // fire.auth().settings.appVerificationDisabledForTesting = true;

    // var phoneNumber = "+62895363620727";
    // var testVerificationCode = "123456";

    // var appVerifier = window.recaptchaVerifier;
    // firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
    // .then(function (confirmationResult) {
    //   // SMS sent. Prompt user to type the code from the message, then sign the
    //   // user in with confirmationResult.confirm(code).
    //   window.confirmationResult = confirmationResult;

    // }).catch(function (error) {
    //   // Error; SMS not sent
    //   // ...
    // });

    // fire.auth().settings.appVerificationDisabledForTesting = true;

   const {phoneNumber} = this.state
    var testVerificationCode = "123456";

    var appVerifier = window.recaptchaVerifier;

    console.log("weyah", appVerifier)
    if(phoneNumber.length > 0){

      fire.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(confirmationResult => {
        this.setState({
          confirmationResult : confirmationResult,
        })
        // return confirmationResult.confirm(testVerificationCode)
      }).catch( (error)=> {
        console.log("error send otp",error)

        this.setState({ message: `Pengiriman OTP bermasalah: ${error.message.trim().toLowerCase().startsWith('an internal error') ? 'Tidak ada koneksi internet' : error.message}` })
        // Error; SMS not sent
        // ...
      });

    }
  


  }

  verify(e) {
    e.preventDefault();

    const { code, confirmationResult } = this.state;

    confirmationResult.confirm(code).then( result=> {
      console.log("hasil verifikasi", result);
      // User signed in successfully.
      var user = result.user;
      this.setState({
        user
      })

      console.log("hasil user", user);
      // ...
    }).catch( (error)=> {
      console.log("error", error)
      this.setState({ message: `Pengiriman OTP bermasalah: ${error.message.trim().toLowerCase().startsWith('an internal error') ? 'Tidak ada koneksi internet' : error.message}` })
      // User couldn't sign in (bad verification code?)
      // ...
    });
  }


  renderMessage() {
    const { message } = this.state;

    if (!message.length) return null;

    return (
     <div> {message} </div>
    );
  }

  render() {
    console.log("state login", this.state)
    console.log("recaptha", this.recaptcha)
    console.log("cek", cek)
    return (
      <div className="col-md-6">
        
        {this.renderMessage()}

        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Login OTP</label>
            <input value={this.state.phoneNumber} onChange={this.handleChange} type="phoneNumber" name="phoneNumber" placeholder="Enter Your Number" />
          </div>

          <div ref={(ref) => this.recaptcha = ref}></div>

          {this.state.recaptcha ? 
            (<button type="submit" onClick={this.login} class="btn btn-primary">Login</button> ) :
            (<div/>)
          }

          {/* {this.state.visible ? ( */}

            <div class="form-group">
              <label for="exampleInputEmail1">Verifikasi OTP</label>
              <input value={this.state.code} onChange={this.handleChange} type="code" name="code"  placeholder="Enter Your Verify Number" />
              <button type="submit" onClick={this.verify} class="btn btn-primary">Verifikasi</button>

            </div>

           {/* ) : (<div />)} */}

        </form>

      </div>
    );
  }
}
export default Login;