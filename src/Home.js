import React, { Component } from 'react';
import fire from './config/Fire';

class Home extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }


    logout() {
        fire.auth().signOut();
    }

    render() {
        return (
          <div>
            Your Home

            <button type="submit" onClick={this.logout} class="btn btn-primary">logout</button>
          </div>
        );

    }

}

export default Home;